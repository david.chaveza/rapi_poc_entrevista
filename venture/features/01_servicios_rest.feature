# language: es
# encoding: utf-8
Característica: SERVICIOS REST
  Basado en los servicios rest de prueba de la pagina https://reqres.in/ ...


  @regresion @ws_01
  Escenario: WS_01 - LIST USERS
    Dado Quiero probar un servicio
    Cuando Pruebo el web services "list users"
    Entonces Valido que el Response Code sea exitoso
    Y Valido en el Response la key: "$.data[0].email" contenga: "michael.lawson@reqres.in"
    Y Valido en el Response la key: "$.data[*].email" contenga: "lindsay.ferguson@reqres.in"

  @regresion @ws_02
  Escenario: WS_02 - CREATE
    Dado Quiero probar un servicio
    Cuando Pruebo el web services "create"
    Entonces Valido que el Response Code sea exitoso
    Y Valido en el Response la key: "$.id" sea numerico
    Y Guardo el valor de la key: "$.id" en variable ID

  @regresion @ws_03
  Escenario: WS_03 - DELETE
    Dado Quiero probar un servicio
    Cuando Pruebo el web services "delete"
    Entonces Valido que el Response Code sea exitoso
