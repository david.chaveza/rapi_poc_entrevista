require_relative '../../../helpers/generic'

When(/^Abro el aplicativo sistema$/) do
  generic_page = GenericPage.new
  generic_page.abrir_app
  save_evidence_execution
end

#******************************************************************
# PLP
#******************************************************************
When(/^Selecciono la categoría "([^"]*)"$/) do |categoria|
  @plp_page = PlpPage.new
  @plp_page.seleccionar_categoria(categoria)
  save_evidence_execution
end

When(/^Selecciono el producto "([^"]*)"$/) do |producto|
  @plp_page.seleccionar_producto(producto)
  save_evidence_execution
end

#******************************************************************
# PDP
#******************************************************************
When(/^Se muestra la pagina 'PDP'$/) do
  sleep 1
  save_evidence_execution
end

When(/^Presiono el botón 'Add to cart'$/) do
  @pdp_page = PdpPage.new
  @pdp_page.agregar_al_carrtio
end

When(/^Se muestra el Alert de producto agregado$/) do
  sleep 1
  save_evidence_execution
end

When(/^Confirmo el Alert de producto agregado$/) do
  @pdp_page.confirmar_alert_producto_agregado
end

#******************************************************************
# HEADERS
#******************************************************************
When(/^Selecciono el menu 'Cart'$/) do
  headers_page = HedersPage.new
  headers_page.seleccionar_menu_cart
end

#******************************************************************
# CART
#******************************************************************
When(/^Se muestra la pagina 'Cart'$/) do
  sleep 1
  save_evidence_execution
end

When(/^Presiono el botón 'Place order'$/) do
  cart_page = CartPage.new
  cart_page.click_place_order
end

#******************************************************************
# CHECKOUT
#******************************************************************
When(/^Se muestra el formulario 'Place order'$/) do
  sleep 1
  save_evidence_execution
end

When(/^Lleno el formulario 'Place order'$/) do
  place_order = PlaceOrderFormPage.new
  place_order.llenar_formulario
  save_evidence_execution
end

When(/^Presiono el botón 'Purchase'$/) do
  place_order = PlaceOrderFormPage.new
  place_order.click_purchase
end

#******************************************************************
# SUCCESS
#******************************************************************
When(/^Se muestra el pop\-up 'Thank you for your purchase!'$/) do
  place_order = PlaceOrderFormPage.new
  place_order.assert_compra_exitosa
  save_evidence_execution
end
