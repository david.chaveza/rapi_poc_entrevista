
When(/^Quiero probar un servicio$/) do
  true
end

When(/^Pruebo el web services "([^"]*)"$/) do |nombre_ws|
  @ws = WS.new(nombre_ws)
  @resp = nil
  @resp = @ws.request
  puts "RESPONSE CODE => \n#{@resp.code}\n\n"
  puts "RESPONSE BODY => \n#{@resp.read_body}\n\n"
  save_evidence_file(@resp.read_body, "response_body", 'json')
end

When(/^Valido que el Response Code sea exitoso$/) do
  @ws.assert_response_code
end

When(/^Valido en el Response la key: "([^"]*)" contenga: "([^"]*)"$/) do |reponse_key, valor_key|
  @ws.val_response_key_value(reponse_key, valor_key)
end

When(/^Valido en el Response la key: "([^"]*)" sea numerico$/) do |key|
  @ws.val_response_key_value_is_numeric(key)
end

When(/^Guardo el valor de la key: "([^"]*)" en variable ID$/) do |key|
  @ws.guardar_variable_id(key)
end