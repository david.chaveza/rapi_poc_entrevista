require_relative '../../../helpers/generic'

When(/^Abro la app$/) do
  puts "DRIVER = #{@@driver}"
  @calc_page = CalcPage.new(@@driver)
  sleep 2
  save_evidence_execution
end

When(/^Presiono el numero "([^"]*)" de la calculadora$/) do |numero|
  @calc_page.presionar_numero(numero)
  save_evidence_execution
  sleep 3
end

When(/^Presiono el signo '\+'$/) do
  @calc_page.presionar_signo_mas
  save_evidence_execution
end

When(/^Presiono el signo '\-'$/) do
  @calc_page.presionar_signo_menos
  save_evidence_execution
end

When(/^Presiono el signo 'X'$/) do
  @calc_page.presionar_signo_multiplicar
  save_evidence_execution
end

When(/^Presiono el signo '\/'$/) do
  @calc_page.presionar_signo_dividir
  save_evidence_execution
end

When(/^Presiono el signo '='$/) do
  @calc_page.presionar_signo_igual
  save_evidence_execution
end

When(/^Valido que el resultado sea: "([^"]*)"$/) do |resultado|
  @calc_page.assert_resultado(resultado)
  save_evidence_execution
end
