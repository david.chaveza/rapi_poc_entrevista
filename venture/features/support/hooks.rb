require_relative '../../../helpers/config.rb'
require_relative '../../../helpers/generic.rb'

#*************************************************************
# BEFORE RUN SUITE
#*************************************************************
AfterConfiguration do |config|
  puts "AFTER CONFIG => #{config.feature_dirs}"
  puts "CODIGO ANTES DE EJECUTAR 1 SUITE"
end

#*************************************************************
# BEFORE SCENARIO
#*************************************************************
Before do |scenario|
  puts "CODIGO ANTES DE EJECUTAR 1 ESCENARIO"
  #Recupera el nombre del feautre, para tomar evidencia dentro de los step
  Config.current_feature = scenario.feature.name
  #Recupera el nombre del scenario, para tomar evidencia dentro de los step
  Config.current_scenario = scenario.name
  #MAXIMIZAR NAVEGADOR
  if Config.driver.to_s.to_sym != :ws and Config.driver.to_s.to_sym != :appium
    page.driver.browser.manage.window.maximize
  end

end

#*************************************************************
# AFTER STEP
#*************************************************************
AfterStep do |scenario|
  puts "CODIGO ANTES DE EJECUTAR 1 STEP"
end

#*************************************************************
# AFTER SCENARIO
#*************************************************************
After do |scenario|
  puts "CODIGO DESPUES DE EJECUTAR 1 ESCENARIO"
  if scenario.failed?
    #EVIDENCIA EN ESTATUS FAIL
    save_evidence_execution_fail
  end

  if Config.driver.to_s.to_sym != :ws and Config.driver.to_s.to_sym != :appium
    Capybara.reset_sessions!
  end
  @@driver.reset if Config.driver.to_s.to_sym == :appium
end

#*************************************************************
# AFTER SUITE
#*************************************************************
at_exit do
  puts "CODIGO DESPUES DE EJECUTAR 1 SUITE"
  @@driver.quit if Config.driver.to_s.to_sym == :appium
  puts "APPIUM DRIVER => QUIT"
end