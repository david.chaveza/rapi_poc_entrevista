require 'em/pure_ruby'
require 'capybara/cucumber'
require 'yaml'

require_relative '../../../helpers/config.rb'

puts "DRIVER => #{Config.driver.to_s.to_sym}"


case Config.driver.to_s.to_sym

when :ws
  puts "DRIVER => WS"

when :firefox
  require 'selenium-webdriver'

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app)
  end

  Capybara.default_driver = :selenium
  Capybara.default_max_wait_time = 5
  Capybara.ignore_hidden_elements = false
  Capybara.reset_sessions!

when :chrome
  require 'selenium-webdriver'

  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

  Capybara.default_driver = :chrome
  Capybara.default_max_wait_time = 10
  Capybara.ignore_hidden_elements = false
  Capybara.reset_sessions!
  Capybara.javascript_driver = :chrome # :chrome simulates behavior in browser

when :appium
  require 'selenium-webdriver'
  require 'appium_lib'

  #****************************************************************************
  # VARIABLES
  #****************************************************************************
  @url_appium_server = "http://127.0.0.1:4723/wd/hub"
  @automation_name = "UiAutomator2"
  @platform_name = "Android"
  @platform_version = "10.0"
  @device_name = "ANDROID DEVICE"
  @device_uid = Config.device_udid
  @path_app = File.expand_path(File.dirname(__FILE__))
  @app_name = 'calc.apk'

  #****************************************************************************
  # CAPABILITIES
  #****************************************************************************
  desired_caps = {
      :caps => {
          :automationName => @automation_name.to_s.strip,
          :platformName => @platform_name.to_s.strip,
          :platformVersion => @platform_version.to_s.strip,
          :deviceName => @device_name.to_s.strip,
          :udid => @device_uid.to_s.strip,
          :app => "#{@path_app}/#{@app_name}"
      },
      :appium_lib => {
          :server_url => @url_appium_server
      }
  }

  #INTANCIAR DRIVER
  @@driver = Appium::Driver.new(desired_caps, true).start_driver
  puts "APPIUM DRIVER = #{@@driver}"


  Appium::Core::Base::Driver
else
  raise("Error. Driver invalido => #{Config.driver}")

end

