# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class CalcPage

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize(driver=Appium::Driver.new)
    @driver = driver
    @btn_numero = { :selector => :xpath, :locator => "//*[@text='${DT_VALUE}']" }
    @btn_signo_igual = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'eq')]" }
    @txt_resultado = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'result_final')]" }
    @btn_signo_mas = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'op_add')]" }
    @btn_signo_menos = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'op_sub')]" }
    @btn_signo_multiplicar = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'op_mul')]" }
    @btn_signo_dividir = { :selector => :xpath, :locator => "//*[contains(@resource-id, 'op_div')]" }

    @wait = 0.5
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def presionar_numero(numero)
    sleep @wait
    locator = String.new(@btn_numero[:locator].to_s)
    locator['${DT_VALUE}'] = numero.to_s.strip unless locator['${DT_VALUE}'].nil?
    numero = @driver.find_element(@btn_numero[:selector], locator)
    numero.click

  rescue Exception => e
    raise "Error. Al presionar el número: '#{numero}'. \nException => #{e.message}"
  end

  def presionar_signo_mas
    sleep @wait
    signo = @driver.find_element(@btn_signo_mas[:selector], @btn_signo_mas[:locator])
    signo.click

  rescue Exception => e
    raise "Error. Al presionar el signo: '+'. \nException => #{e.message}"
  end

  def presionar_signo_menos
    sleep @wait
    signo = @driver.find_element(@btn_signo_menos[:selector], @btn_signo_menos[:locator])
    signo.click

  rescue Exception => e
    raise "Error. Al presionar el signo: '-'. \nException => #{e.message}"
  end

  def presionar_signo_multiplicar
    sleep @wait
    signo = @driver.find_element(@btn_signo_multiplicar[:selector], @btn_signo_multiplicar[:locator])
    signo.click

  rescue Exception => e
    raise "Error. Al presionar el signo: 'X'. \nException => #{e.message}"
  end

  def presionar_signo_dividir
    sleep @wait
    signo = @driver.find_element(@btn_signo_dividir[:selector], @btn_signo_dividir[:locator])
    signo.click

  rescue Exception => e
    raise "Error. Al presionar el signo: '/'. \nException => #{e.message}"
  end

  def presionar_signo_igual
    sleep @wait
    signo = @driver.find_element(@btn_signo_igual[:selector], @btn_signo_igual[:locator])
    signo.click

  rescue Exception => e
    raise "Error. Al presionar el signo: '='. \nException => #{e.message}"
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_resultado(resultado)
    txt_resultado = @driver.find_element(@txt_resultado[:selector], @txt_resultado[:locator])
    unless txt_resultado.attribute('text').to_s.include?(resultado)
      raise "Error. Se esperaba el resultado: '#{resultado}', valor obtenido: '#{txt_resultado.attribute('text').to_s}'"
    end
  end


end
