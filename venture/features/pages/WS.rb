# encoding: utf-8
require "uri"
require 'net/http'
require 'json'
require 'json_path'
require_relative '../../../helpers/generic'
require_relative '../../../helpers/file_data'
require_relative '../../../helpers/csv_data'

class WS

  #############################################################################
  # WEBSERVICE DATA
  #############################################################################
  def initialize(nombre_ws)
    csv = get_csv_data('dt_ws.csv')
    @data = get_data_by_filters("nombre_ws = #{nombre_ws}", csv)[0]
    @url_servicio = @data[:url]
    @dt_query_params = @data[:query_params].to_s
    @dt_request_body = get_file_data(@data[:archivo_request_body].to_s) unless @data[:archivo_request_body].nil?
    @request_metodo = @data[:metodo_http].to_s.to_sym
    @request_ssl = true
    @request_timeout = 60
    @request_headers = [
        {:header => 'Accept', :valor => 'application/json'}
    ]
    @response = nil
    @response_codigo_esperado = @data[:response_code_esperado]
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def request
    #0 - Agregar query params a URL
    unless @data[:query_params].nil?
      q_params = query_params(@data[:query_params])
      @url_servicio = "#{@url_servicio}/#{q_params}"
    end

    # 1 - URI, SSL, TimeOut
    url = URI(@url_servicio)
    https = Net::HTTP.new(url.host, url.port);
    https.use_ssl = @request_ssl
    https.read_timeout = @request_timeout

    # 2 - Métodos HTTP
    request = nil
    request = get_method_request_net_http(url, @request_metodo)

    # 3 - Agregar Headers
    for i in 0..(@request_headers.size - 1)
      request[ @request_headers[i][:header] ] = @request_headers[i][:valor]
    end

    # 4 - Agregar Cuerpo de la respuesta
    unless @data[:archivo_request_body].nil?
      request.body = params_body_request( @dt_request_body.to_s)
    end

    # 5 - Obtener HTTP Response
    @response = https.request(request)

    return @response

  rescue Exception => e
    raise("Error al ejecutar el Request a la URL => '#{@url_servicio}'. \nException => #{e.message}")

  end

  def params_body_request(body_request)
    body_request['${DT_NOMBRE}'] = "#{timestamps}" unless body_request['${DT_NOMBRE}'].nil?
    body_request['${DT_EMAIL}'] = email_random unless body_request['${DT_EMAIL}'].nil?
    return body_request
  end

  def query_params(q_params)
    q_params['${ID}'] = "#{@@ID}" unless q_params['${ID}'].nil?
    return q_params
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_response_code
    unless @response.code.to_s.to_i == @response_codigo_esperado.to_s.to_i
      raise("Error. Codigo de respuesta esperado => #{@response_codigo_esperado} Codigo de respuesta obtenido #{@response.code}")
    end
  end

  def val_response_key_value(key, value)
    resp_json = JSON.parse(@response.read_body)
    #obtener ruta key
    path = JsonPath.new(key)
    valor_actual = path.on(resp_json)

    unless valor_actual.to_s.include?(value.to_s)
      raise("Error. No se encuentra la Key: '#{key}' con el valor: '#{value}'")
    end
  end

  def val_response_key_value_is_numeric(key)
    resp_json = JSON.parse(@response.read_body)
    #obtener ruta key
    path = JsonPath.new(key)
    valor_actual = path.on(resp_json)[0]

    unless is_numeric?(valor_actual)
      raise("Error. La Key: '#{key}'. No es un valor Numerico")
    end
  end

  def guardar_variable_id(key)
    resp_json = JSON.parse(@response.read_body)
    #obtener ruta key
    path = JsonPath.new(key)
    valor_actual = path.on(resp_json)[0]
    @@ID = valor_actual
  end


end


