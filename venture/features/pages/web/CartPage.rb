# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class CartPage

  include Capybara::DSL

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @btn_place_order = { :selector => :xpath, :locator => "//button[text()='Place Order']" }
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def click_place_order
    btn_place_order = find(@btn_place_order[:selector], @btn_place_order[:locator])
    btn_place_order.click

  rescue Exception => e
    raise "Error. Al presionar el boton: 'Place Order'. \nException => #{e.message}"
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################



end
