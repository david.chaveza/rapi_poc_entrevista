# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class PdpPage

  include Capybara::DSL

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @btn_agregar_carrito = { :selector => :xpath, :locator => "//a[text()='Add to cart']" }
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def agregar_al_carrtio
    btn_carrito = find(@btn_agregar_carrito[:selector], @btn_agregar_carrito[:locator])
    btn_carrito.click

  rescue Exception => e
    raise "Error. Al intentar presionar el botón carrito. \nException => #{e.message}"
  end

  def confirmar_alert_producto_agregado
    page.driver.browser.switch_to.alert.accept

  rescue Exception => e
    puts "Error. Al confirmar Alert de producto agregado. \nException => #{e.message}"
  end

  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################



end
