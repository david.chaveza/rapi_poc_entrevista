# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class PlpPage

  include Capybara::DSL

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @menu_categoria = { :selector => :xpath, :locator => "//a[@id='itemc' and contains(text(), '${DT_VALUE}')]" }
    @lbl_nombre_producto = { :selector => :xpath, :locator => "//a[text()='${DT_VALUE}']" }
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def seleccionar_categoria(categoria)
    locator = String.new(@menu_categoria[:locator].to_s)
    locator['${DT_VALUE}'] = categoria.to_s.strip unless locator['${DT_VALUE}'].nil?
    menu_categoria = find(@menu_categoria[:selector], locator)
    menu_categoria.click

  rescue Exception => e
    raise "Error. Al intentar seleccionar la categoria: '#{categoria}'. \nException => #{e.message}"
  end

  def seleccionar_producto(producto)
    locator = String.new(@lbl_nombre_producto[:locator].to_s)
    locator['${DT_VALUE}'] = producto.to_s.strip unless locator['${DT_VALUE}'].nil?
    producto = find(@lbl_nombre_producto[:selector], locator)
    producto.click

  rescue Exception => e
    raise "Error. Al intentar seleccionar el producto: '#{producto}'. \nException => #{e.message}"
  end



  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################



end
