# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class HedersPage

  include Capybara::DSL

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @menu_cart = { :selector => :id, :locator => "cartur" }
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def seleccionar_menu_cart
    menu_cart = find(@menu_cart[:selector], @menu_cart[:locator])
    menu_cart.click

  rescue Exception => e
    raise "Error. Al seleccionar el menu: 'Cart'. \nException => #{e.message}"
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################



end
