# encoding: utf-8
require 'fileutils'
require 'rspec'
require 'rspec/expectations'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class PlaceOrderFormPage

  include Capybara::DSL
  include RSpec::Matchers

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @txt_name = { :selector => :id, :locator => "name" }
    @txt_country = { :selector => :id, :locator => "country" }
    @txt_city = { :selector => :id, :locator => "city" }
    @txt_card = { :selector => :id, :locator => "card" }
    @txt_month = { :selector => :id, :locator => "month" }
    @txt_year = { :selector => :id, :locator => "year" }
    @btn_purchase = { :selector => :xpath, :locator => "//button[@onclick='purchaseOrder()']" }

    #POP UP EXITOSO
    @lbl_compra_exitosa = { :selector => :xpath, :locator => "//h2[text()='Thank you for your purchase!']" }

  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def llenar_formulario
    txt_name = find(@txt_name[:selector], @txt_name[:locator])
    txt_name.set('TEST')
    txt_country = find(@txt_country[:selector], @txt_country[:locator])
    txt_country.set('MX')
    txt_city = find(@txt_city[:selector], @txt_city[:locator])
    txt_city.set('CDMX')
    txt_card = find(@txt_card[:selector], @txt_card[:locator])
    txt_card.set('4444111111111111')
    txt_month = find(@txt_month[:selector], @txt_month[:locator])
    txt_month.set('12')
    txt_year = find(@txt_year[:selector], @txt_year[:locator])
    txt_year.set('22')

  rescue Exception => e
    raise "Error. Al llenar el formulario: 'Place Order'. \nException => #{e.message}"
  end

  def click_purchase
    btn_purchase = find(@btn_purchase[:selector], @btn_purchase[:locator])
    btn_purchase.click

  rescue Exception => e
    raise "Error. Al presionar el boton: 'Purchase'. \nException => #{e.message}"
  end


  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################

  def assert_compra_exitosa
    page.should have_xpath(@lbl_compra_exitosa[:locator])
  end


end
