# encoding: utf-8
require 'fileutils'

require_relative '../../../../helpers/generic'
require_relative '../../../../helpers/config'

class GenericPage

  include Capybara::DSL

  #############################################################################
  # WEB ELEMENTS
  #############################################################################
  def initialize
    @url_base = Config.url_base
  end

  #############################################################################
  # ACCIONES
  #############################################################################

  def abrir_app
    visit @url_base
  end



  #############################################################################
  # ASSERTS/VERIFYS/VALIDACIONES
  #############################################################################



end
