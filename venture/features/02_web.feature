# language: es
# encoding: utf-8
Característica: WEB - COMPRA
  Compra de productro en https://www.demoblaze.com/index.html#


  @regresion @web_01
  Escenario: WEB_01 - COMPRA PRODUCTO LAPTOP
    Dado Abro el aplicativo sistema
    Cuando Selecciono la categoría "Laptops"
    Y Selecciono el producto "Dell i7 8gb"
    Entonces Se muestra la pagina 'PDP'
    Y Presiono el botón 'Add to cart'
    Entonces Se muestra el Alert de producto agregado
    Cuando Confirmo el Alert de producto agregado
    Y Selecciono el menu 'Cart'
    Entonces Se muestra la pagina 'Cart'
    Cuando Presiono el botón 'Place order'
    Entonces Se muestra el formulario 'Place order'
    Cuando Lleno el formulario 'Place order'
    Y Presiono el botón 'Purchase'
    Entonces Se muestra el pop-up 'Thank you for your purchase!'

  @regresion @web_02
  Escenario: WEB_02 - COMPRA PRODUCTO PHONES
    Dado Abro el aplicativo sistema
    Cuando Selecciono la categoría "Phones"
    Y Selecciono el producto "Nexus 6"
    Entonces Se muestra la pagina 'PDP'
    Y Presiono el botón 'Add to cart'
    Entonces Se muestra el Alert de producto agregado
    Cuando Confirmo el Alert de producto agregado
    Y Selecciono el menu 'Cart'
    Entonces Se muestra la pagina 'Cart'
    Cuando Presiono el botón 'Place order'
    Entonces Se muestra el formulario 'Place order'
    Cuando Lleno el formulario 'Place order'
    Y Presiono el botón 'Purchase'
    Entonces Se muestra el pop-up 'Thank you for your purchase!'

