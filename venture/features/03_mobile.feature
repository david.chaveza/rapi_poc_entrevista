# language: es
# encoding: utf-8
Característica: MOBILE - CALCULADORA

  @regresion @adr_01
  Escenario: ADR_01 - SUMA
    Dado Abro la app
    Cuando Presiono el numero "3" de la calculadora
    Y Presiono el signo '+'
    Cuando Presiono el numero "7" de la calculadora
    Y Presiono el signo '='
    Entonces Valido que el resultado sea: "10"

  @regresion @adr_02
  Escenario: ADR_02 - RESTA
    Dado Abro la app
    Cuando Presiono el numero "1" de la calculadora
    Cuando Presiono el numero "0" de la calculadora
    Y Presiono el signo '-'
    Cuando Presiono el numero "7" de la calculadora
    Y Presiono el signo '='
    Entonces Valido que el resultado sea: "3"

  @regresion @adr_03
  Escenario: ADR_03 - MULTIPLICAR
    Dado Abro la app
    Cuando Presiono el numero "7" de la calculadora
    Y Presiono el signo 'X'
    Cuando Presiono el numero "3" de la calculadora
    Y Presiono el signo '='
    Entonces Valido que el resultado sea: "21"

  @regresion @adr_04
  Escenario: ADR_04 - DIVIDIR
    Dado Abro la app
    Cuando Presiono el numero "9" de la calculadora
    Y Presiono el signo '/'
    Cuando Presiono el numero "3" de la calculadora
    Y Presiono el signo '='
    Entonces Valido que el resultado sea: "3"