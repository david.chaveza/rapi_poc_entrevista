#####################################################
#Autor: BICHO, TETE
#Descripcion:  Helper de configuracion general
#####################################################

class Config

  #************************************************************************************
  # VARIABLES
  #************************************************************************************
  #Driver de la ejecucion selenium / chromedriver / appium
  @driver = ENV['DRIVER'].nil? ? 'ws' : ENV['DRIVER'].to_s.downcase.strip
  #udid device
  @device_udid = ENV['DEVICE_UDID'].nil? ? "ZY225C4DM8" : ENV['DEVICE_UDID'].to_s.strip
  #url base del ambiente a ejecutar
  @url_base = ENV['URL_BASE'].nil? ? 'https://www.demoblaze.com/index.html#' : ENV['URL_BASE'].to_s.strip
  #Fecha de ejecucion este valor podria ser usado para crear la carpeta que contenga reporte y evidencia.
  @exec_date = Time.now.strftime("%Y%m%d")
  #Hora de ejecucion este valor podria ser usado para crear la carpeta que contenga reporte y evidencia.
  @exec_time = Time.now.strftime("%H%M")
  #obtiene el directorio de resultados seteado como variable de entorno
  #   de no existir la variable se usa un directorio default
  @path_results = ENV['PATH_RESULTS'].nil? ? "evidencia/#{@exec_date}/#{@exec_time}" : "#{ENV['PATH_RESULTS'].to_s.downcase.strip}"
  @path_evidencia = "#{@path_results}/evidencia"


  #************************************************************************************
  # VARIABLES TEST
  #************************************************************************************
  @current_feature = nil
  @current_scenario = nil
  @dt_data = Array.new
  @dt_row = nil
  @vars_out = Array.new

  #****************************************************
  #CLASS SELF
  #****************************************************
  class << self
    attr_accessor :driver, :url_base, :exec_time, :exec_date, :path_results, :current_feature, :current_scenario,
                :path_evidencia, :dt_data, :dt_row, :vars_out, :device_udid

    def to_s
      s = StringIO.new
      s << "@driver = #{@driver}, "
      s << "@url_base = #{@url_base}, "
      s << "@device_udid = #{@device_udid}, "
      s << "@exec_date = #{@exec_date}, "
      s << "@exec_time = #{@exec_time}, "
      s << "@path_results = #{@path_results}, "
      s << "@current_feature = #{@current_feature}, "
      s << "@current_scenario = #{@current_scenario}, "
      s << "@path_evidencia = #{@path_evidencia}"
      s << "@dt_data = #{@dt_data}"
      s << "@dt_row = #{@dt_row}"
      s << "@vars_out = #{@vars_out}"
      return s.string.to_s.strip
    end


    ##################################################################################
    ### VARIABLES OUT
    ### 		Permite obtener y crear variables que se podran usar a futuro en otro test
    ##################################################################################

    #Método para obtener el valor de una variable
    # @params
    # * :var_name String nombre de la variable
    def get_var_out(var_name)
      value = String.new
      for i in 0..(@vars_out.size - 1)
        if @vars_out[i][:var_name].to_s.downcase.strip == var_name.to_s.downcase.strip
          value = @vars_out[i][:value].to_s
          break
        end
      end
      return value
    rescue Exception => e
      raise "get_var_out => No se pudo obtener la variable '#{var_name}' \nException => #{e.message}"
    end

    #Método para crear una variable
    # @params
    # * :var_name String nombre de la variable
    # * :value String valor de la variable
    def set_var_out(var_name, value=nil)
      var_exist = false

      #buscamos si existe la variable sobreescribimos el valor
      for i in 0..(@vars_out.size - 1)
        if @vars_out[i][:var_name].to_s.downcase.strip == var_name.to_s.downcase.strip
          @vars_out[i][:value] = value
          var_exist = true
          break
        end
      end

      #si no existe entonces cramos nueva variable y agregamos al array
      unless var_exist
        var = { :var_name => var_name.to_s.strip, :value => value.to_s.strip}
        @vars_out.push(var)
      end

    rescue Exception => e
      raise "set_var_out => No se pudo crear la variable '#{var_name}' con el valor '#{value}' \nException => #{e.message}"
    end


  end

end
