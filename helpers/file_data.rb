require 'fileutils'
require 'yaml'


#Metodo obtener un archivo completo
# @params:
#   :file_name nobre del archivo  que se va cargar
def get_file_data(file_name)
  file = nil
  data = StringIO.new

  #obtiene el path y nombre del archivo csv
  file =  File.join(File.dirname(__FILE__), "../venture/config/data/#{file_name}")

  #lee el archivo
  File.open(file).each do |line|
    data << line
  end

  return data.string.to_s

end

