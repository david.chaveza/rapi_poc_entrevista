require 'fileutils'
require 'cucumber'
require 'cucumber/formatter/console'
require 'cucumber/formatter/html'
require_relative 'config'


##################################################################################
### UTILERIAS
##################################################################################

#Método que obtiene un numero unico apartir del timestamps
def timestamps
  (Time.now.to_f * 1000).to_i
end

#Método que obtiene la fecha en formato HORA,MINUTO,SEGUNDO
def datetime
  Time.now.strftime('%H%M%S').to_s
end

#Método que obtiene la fecha en formato HORA,MINUTO,SEGUNDO
def date
  Time.now.strftime('%Y%m%d').to_s
end

#Método que genera un email de forma random, apartir de la fecha
def email_random
  return "test.#{timestamps}@test.com"
end


#Método que quita caracteres especiales a un string
def special_chars_replace(string_value)
  string_value = string_value.to_s.tr(':', '')
  string_value = string_value.to_s.tr('-', '')
  string_value = string_value.to_s.tr(',', '') # ,
  string_value = string_value.to_s.tr('.', '')
  string_value = string_value.to_s.tr('"', '')
  string_value = string_value.to_s.tr( ';', '' )
  string_value = string_value.to_s.tr( '(', '' )
  string_value = string_value.to_s.tr( ')', '' )
  string_value = string_value.to_s.tr( '{', '' )
  string_value = string_value.to_s.tr( '}', '' )
  string_value = string_value.to_s.tr( '[', '' )
  string_value = string_value.to_s.tr( ']', '' )
  string_value = string_value.to_s.tr( '!', '' )
  string_value = string_value.to_s.tr( '@', '' )
  string_value = string_value.to_s.tr( '#', '' )
  string_value = string_value.to_s.tr( '$', '' )
  string_value = string_value.to_s.tr( '%', '' )
  string_value = string_value.to_s.tr( '&', '' )
  string_value = string_value.to_s.tr( '/', '' )
  string_value = string_value.to_s.tr( '?', '' )
  string_value = string_value.to_s.tr( '¿', '' )
  string_value = string_value.to_s.tr( '=', '' )
  string_value = string_value.to_s.tr( '>', '' )
  string_value = string_value.to_s.tr( 'á', 'a' )
  string_value = string_value.to_s.tr( 'é', 'e' )
  string_value = string_value.to_s.tr( 'í', 'i' )
  string_value = string_value.to_s.tr( 'ó', 'o' )
  string_value = string_value.to_s.tr( 'ú', 'u' )
  string_value = string_value.to_s.tr( ' ', '_' )
  return string_value.to_s.downcase.strip
end

def is_numeric?(obj)
  obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
end

##################################################################################
### EVIDENCIA - FILE
##################################################################################

#Método para guardar 1 archivo de evidencia
def save_evidence_file(text_file, file_name = nil, file_extencion = nil)
  feature_name = special_chars_replace(Config.current_feature.to_s.downcase.strip)
  scenario_name = special_chars_replace(Config.current_scenario.to_s.downcase.strip)
  evidence_dir = "#{Config.path_results}/evidencia/#{feature_name}/#{scenario_name.to_s}"

  #crear nombre y extencion de archivo
  file_name = file_name.nil? ? "evidence_file" : file_name
  file_extencion = file_extencion.nil? ? "txt" : file_extencion
  evidence_file_name = "#{file_name}.#{file_extencion}"

  #crear directorio de evidencia
  FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir

  #escribir archivo
  File.open("#{evidence_dir}/#{evidence_file_name}", 'w') { |file| file.write(text_file.to_s.strip) }
  #agregar archivo a reporte cucumber
  embed(File.expand_path("#{evidence_dir}/#{evidence_file_name.to_s}"), 'text/plain', "<li class=\"step message\">FILE EVIDENCE: #{evidence_file_name.to_s}</li>")

rescue Exception => e
  puts "Error => save_evidence_file => No se pudo crear archivo de evidencia #{e.message}"

end



##################################################################################
### EVIDENCIA - SCREENSHOTS
##################################################################################

#Método para guardar la evidencia de ejecucion al momento de invocar este metodo, en este punto toda la evidencia es pass
def save_evidence_execution
  begin
    feature_name = special_chars_replace(Config.current_feature.to_s.downcase.strip)
    scenario_name = special_chars_replace(Config.current_scenario.to_s.downcase.strip)

    evidence_dir = "#{Config.path_results}/evidencia/#{feature_name}/#{scenario_name.to_s}"
    evidence_name = "#{timestamps}_pass.png"
    FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir

    if Config.driver.to_s.to_sym == :appium
      @@driver.save_screenshot("#{evidence_dir}/#{evidence_name}")
    else
      page.save_screenshot("#{evidence_dir}/#{evidence_name}", :full => true)
    end
    embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png', "<li class=\"step message\">SCREENSHOT: #{evidence_name.to_s}</li>")

  rescue Exception => e
    puts "Error => save_evidence_execution => #{e.message}"
  end
end

#Método para guardar la evidencia de ejecucion al momento de invocar este metodo, en este punto toda la evidencia es pass
def save_evidence_execution_fail
  begin
    feature_name = special_chars_replace(Config.current_feature.to_s.downcase.strip)
    scenario_name = special_chars_replace(Config.current_scenario.to_s.downcase.strip)

    evidence_dir = "#{Config.path_results}/evidencia/#{feature_name}/#{scenario_name.to_s}"
    evidence_name = "#{timestamps}_fail.png"

    FileUtils::mkdir_p evidence_dir unless Dir.exist? evidence_dir
    if Config.driver == :appium
      @@driver.screenshot("#{evidence_dir}/#{evidence_name}")
    else
      page.save_screenshot("#{evidence_dir}/#{evidence_name}", :full => true)
    end
    embed(File.expand_path("#{evidence_dir}/#{evidence_name.to_s}"), 'image/png', "#{evidence_name.to_s}")

  rescue Exception => e
    puts "Error => save_evidence_execution => #{e.message}"
  end
end


##################################################################################
### NET HTTP - UTILIERIAS
##################################################################################
def get_method_request_net_http(url, method)
  require 'net/http'
  request = nil

  case method.to_s.to_sym
    when :get then
      request = Net::HTTP::Get.new(url)
    when :post then
      request = Net::HTTP::Post.new(url)
    when :put then
      request = Net::HTTP::Put.new(url)
    when :delete then
      request = Net::HTTP::Delete.new(url)
    when :patch then
      request = Net::HTTP::Patch.new(url)
    else
      raise("method request => opción http method invalida => #{method}")
  end

  return request
end