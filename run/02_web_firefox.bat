REM @ECHO OFF

REM ******************************************************************************************
REM VARIABLES
REM ******************************************************************************************
REM NOMBRE DEL DRIVER A UTILIZAR firefox/chrome/chrome_headless
set DRIVER=firefox
REM URL DEL AMBIENTE
set URL_BASE=https://www.demoblaze.com/index.html#
REM ID DEL DISPOSITIVO
set DEVICE_UDID=ZY225C4DM8
REM NOMBRE DEL CUCUMBER PROFILE EN "../VENTURE/CONFIG/CUCUMBER.YML"
set CUCUMBER_PROFILE="web"
REM NOMBRE DEL ARCHIVO FEATURE A EJECUTAR
set FEATURE="02_web.feature"
REM TAGS A FILTRAR EN LA EJECUCIÓN
set TAGS=--tags "@regresion"
REM VARIABLES PARA DETERMINAR EL NOMBRE DE LAS CARPETAS DE EVIDENCIA FECHA Y HORA DE EJECUCIÓN
set EXEC_HOUR=%time:~0,2%
if %EXEC_HOUR% leq 9 set EXEC_HOUR=0%EXEC_HOUR:~1,1%
set EXEC_MIN=%time:~3,2%
if %EXEC_MIN% leq 9 set EXEC_MIN=0%EXEC_MIN:~1,1%
REM VARIABLE QUE DETERMINA EL DIRECTORIO FINAL DE RESULTADOS
set PATH_RESULTS=evidencia/%date:~-4%%date:~3,2%%date:~0,2%/%EXEC_HOUR%%EXEC_MIN%/%DRIVER%

REM ******************************************************************************************
REM IR A DIRECTORIO DONDE ESTA LA CARPETA FEATURES
REM ******************************************************************************************
cd ../
cd venture

REM ******************************************************************************************
REM CREAR DIRECTORIO DE RESULTADOS "PATH_RESULTS"
REM ******************************************************************************************
mkdir "%PATH_RESULTS%"

REM ******************************************************************************************
REM EJECUTAR PROYECTO CUCUMBER
REM ******************************************************************************************
bundle exec cucumber %TAGS% -p %CUCUMBER_PROFILE% features/%FEATURE% -f pretty -f html --out %PATH_RESULTS%/reporte.html

pause

exit
