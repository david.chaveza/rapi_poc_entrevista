-------------------------------------
# RAPI TEST DEMO

    - Esta es una demo de automation test, la cual puede 
      tener bastantes puntos de mejora para un proyecto real

-------------------------------------
# Precondiciones
-------------------------------------

    - Instalar ruby 2.6
        - Windows: RubyInstaller
        - Unix / Mac Os: RVM
    - Instalar JDK 1.8 o superior
        - Crear variable de entorno JAVA_HOME
        - Agregar a variable de entorno PATH el valor:
            - Windows: %JAVA_HOME%/bin
            - Unix / Mac Os: $JAVA_HOME/bin
    - SDK Android 
        - Crear variable de entorno ANDROID_HOME
        
-------------------------------------
# Ejecutar Test
-------------------------------------
1- Ir al path del proyeto

    cd $path_proyect

2- Ejecutar bundle install

    bundle install --path=bundle

3- Ir a $path_proyect/run

    cd $path_proyect/run

4- Ejecutar los test desde los .bat

    - ws.bat: test webservices https://reqres.in/
    - web.bat: test web https://www.demoblaze.com/index.html#
        - web_chrome.bat
        - web_firefox.bat
    - mobile.bat: test calculadora
        - Precondición: Levantar manualmente la aplicación Appium Server
        **Nota: Se aplico en dispositivo fisico o AVD Emulador. 
                Cambiar la variable DEVICE_UUID en el .bat
                por el ID del dispositivo correspondiente, 
                usando adb devices
5- Reportes
    
    - Dentro de la carpeta $PROYECTO/venture/evidencia/
        se crea una estrucutra de carpetas con la fecha y hora de ejecución
        driver ejecutado, feature y test correspondientes
        aqui podemos encontrar:
            - reporte html cucumber
            - evidencia ordenada de cada test ejecutado

# SIGUIENTES PASOS
-------------------------------------
1- Web Services:

    - Parametrizar Headers desde el CSV
    - Incremetar los asserts genericos para evaluar si un campo es:
        numerico, texto, boleano, etc
    - El test "WS_02 - CREATE" se ha hardcodeado crear la variable ID,
        esto puede ser mejorado creando y almacenando variables con nombre dinamicos
        y serviria no solo para webservices, si no en general para el resto de plataformas

2- Web:

    - No se crearón asserts adecuadamente para validar que las page o elementos se muestren,
        se puede implementar estas assertions
    - Separar el mapeo de webelements con las acciones a nivel usuario sobre la page
    - las acciones se mandan de manera nativa, se puede implementar sobreescritura de acciones,
        o crear métodos especificios para ejecutar las acciones y realizar llamadas extra
        como escritura a logs, validacion de condiciones esperadas para los elementos antes
        de hacer la acción.
    - Se pasan los parametros desde el feature, sin embargo se podrían usar
        data driven desde excel, csv, bd segun la necesidad del proyecto
      
3- Mobile:

    - Mediante shell se puede crar un script para: 
        - crear la AVD Manager
        - iniciar el emulador y esperar a que cargue
        - ejecutar los test sobre el emulador
    - Se puede implementar la llamada automatica de Appium Server de forma automatica.
    - las acciones se mandan de manera nativa, se puede implementar sobreescritura de acciones,
        o crear métodos especificios para ejecutar las acciones y realizar llamadas extra
        como escritura a logs, validacion de condiciones esperadas para los elementos antes
        de hacer la acción.
    - La llamadas a los touch de los signos y numeros son de a 1 botón x botón
        se puede implementar un Step que envie el parametro completo de la operación a realizar
        por ejemplo: "2x2+5" y posterior en la page adecuar el codigo para que 
        todos los caracteres.
    - La app apk, se dejo en un directorio nada practico, se puede mover a un directorio
        llamado apps en la raiz del proyecto

4- General:

    - Implementar Logs
    - Implementar retry o rerun de cucumber
    - Implementar en esquema CI/CD
    - Implementar reportes customizados
    - Implementar el uso de más de un driver en paralelo para robustecer 
        la potencia de la herramienta
        
        
               